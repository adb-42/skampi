{{ if .Values.rest.enabled }}
---
apiVersion: v1
kind: Service
metadata:
  name: {{ template "oet.name" . }}-{{ .Values.rest.component }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "oet.labels" . | indent 4 }}
    component: {{ .Values.rest.component }}
    function: {{ .Values.rest.function }}
    domain: {{ .Values.rest.domain }}
    intent: production
spec:
  ports:
  - name: oet-rest
    port: 5000
  clusterIP: None
  selector:
    app: {{ template "oet.name" . }}
    component: {{ .Values.rest.component }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ template "oet.name" . }}-{{ .Values.rest.component }}-scripts
  namespace: {{ .Release.Namespace }}
binaryData:
  hello_world.py: |-
{{ .Files.Get "data/hello_world.py" | b64enc | indent 4 }}
  allocate.py: |-
{{ .Files.Get "data/allocate.py" | b64enc | indent 4 }}
  allocate_from_file.py: |-
{{ .Files.Get "data/allocate_from_file.py" | b64enc | indent 4 }}
  allocate_from_file_sb.py: |-
{{ .Files.Get "data/allocate_from_file_sb.py" | b64enc | indent 4 }}
  deallocate.py: |-
{{ .Files.Get "data/deallocate.py" | b64enc | indent 4 }}
  startup.py: |-
{{ .Files.Get "data/startup.py" | b64enc | indent 4 }}
  standby.py: |-
{{ .Files.Get "data/standby.py" | b64enc | indent 4 }}
  observe.py: |-
{{ .Files.Get "data/observe.py" | b64enc | indent 4 }}
  observe_sb.py: |-
{{ .Files.Get "data/observe_sb.py" | b64enc | indent 4 }}
  abort.py: |-
{{ .Files.Get "data/abort.py" | b64enc | indent 4 }}
  restart.py: |-
{{ .Files.Get "data/restart.py" | b64enc | indent 4 }}
  reset.py: |-
{{ .Files.Get "data/reset.py" | b64enc | indent 4 }}
  example_allocate.json: |-
{{ .Files.Get "data/example_allocate.json" | b64enc | indent 4 }}
  example_configure.json: |-
{{ .Files.Get "data/example_configure.json" | b64enc | indent 4 }}
  example_sb.json: |-
{{ .Files.Get "data/example_sb.json" | b64enc | indent 4 }}
  long_sb.json: |-
{{ .Files.Get "data/long_sb.json" | b64enc | indent 4 }}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ template "oet.name" . }}-{{ .Values.rest.component }}
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "oet.labels" . | indent 4 }}
    component: {{ .Values.rest.component }}
    function: {{ .Values.rest.function }}
    domain: {{ .Values.rest.domain }}
    intent: production
spec:
  selector:
    matchLabels:
      app: {{ template "oet.name" . }}
      component: {{ .Values.rest.component }}
  serviceName: rest-{{ template "oet.name" . }}-{{ .Release.Name }}
  replicas: 1
  template:
    metadata:
      labels:
        {{- include "oet.labels" . | indent 8 }}
        component: {{ .Values.rest.component }}
        function: {{ .Values.rest.function }}
        domain: {{ .Values.rest.domain }}
        intent: production
    spec:
      initContainers:
      - name: check-databaseds-ready
        image: "{{ .Values.rest.image.registry }}/{{ .Values.rest.image.image }}:{{ .Values.rest.image.tag }}"
        command:
          - /usr/local/bin/wait-for-it.sh
          - {{ if .Values.tangoDatabaseDS -}} {{ .Values.tangoDatabaseDS }}:10000 {{- else -}} databaseds-tango-base-{{ .Release.Name }}:10000 {{- end }}
          - --timeout=30
          - --strict
          - --
          - echo databaseds ready
      containers:
      - name: oet-rest
        image: "{{ .Values.rest.image.registry }}/{{ .Values.rest.image.image }}:{{ .Values.rest.image.tag }}"
        imagePullPolicy: {{ .Values.rest.image.pullPolicy }}
        command: ["/bin/sh"]
        args: ["-c", "/$HOME/.local/bin/flask run -h 0.0.0.0"]
        env:
          - name: TANGO_HOST
            value: {{ if .Values.tangoDatabaseDS -}} {{ .Values.tangoDatabaseDS }}:10000 {{- else -}} databaseds-tango-base-{{ .Release.Name }}:10000 {{- end }}
          - name: FLASK_APP
            value: /app/oet/procedure/application/restserver:create_app
          - name: SKUID_URL
            value: "skuid-skuid-{{ .Release.Namespace }}-{{ .Release.Name }}.{{ .Release.Namespace }}.svc.cluster.local:9870"
        ports:
          - name: oet-rest
            containerPort: 5000
        volumeMounts:
          - name: oet-scripts-volume
            mountPath: /scripts
        readinessProbe:
          tcpSocket:
            port: 5000
          initialDelaySeconds: {{ .Values.rest.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.rest.readinessProbe.periodSeconds }}
          timeoutSeconds: {{ .Values.rest.readinessProbe.timeoutSeconds }}
          successThreshold: {{ .Values.rest.readinessProbe.successThreshold }}
          failureThreshold: {{ .Values.rest.readinessProbe.failureThreshold }}
        livenessProbe:
          tcpSocket:
            port: 5000
          initialDelaySeconds: {{ .Values.rest.livenessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.rest.livenessProbe.periodSeconds }}
          timeoutSeconds: {{ .Values.rest.livenessProbe.timeoutSeconds }}
          successThreshold: {{ .Values.rest.livenessProbe.successThreshold }}
          failureThreshold: {{ .Values.rest.livenessProbe.failureThreshold }}
      volumes:
        - name: oet-scripts-volume
          configMap:
            name: {{ template "oet.name" . }}-{{ .Values.rest.component }}-scripts
            items:
              - key: hello_world.py
                path: hello_world.py
              - key: allocate.py
                path: allocate.py
              - key: allocate_from_file.py
                path: allocate_from_file.py
              - key: allocate_from_file_sb.py
                path: allocate_from_file_sb.py
              - key: deallocate.py
                path: deallocate.py
              - key: startup.py
                path: startup.py
              - key: standby.py
                path: standby.py
              - key: observe.py
                path: observe.py
              - key: observe_sb.py
                path: observe_sb.py
              - key: abort.py
                path: abort.py
              - key: restart.py
                path: restart.py
              - key: reset.py
                path: reset.py
              - key: example_allocate.json
                path: example_allocate.json
              - key: example_configure.json
                path: example_configure.json
              - key: example_sb.json
                path: example_sb.json
              - key: long_sb.json
                path: long_sb.json
      resources:
  {{ toYaml .Values.rest.resources | indent 10 }}
  {{- with .Values.nodeSelector }}
nodeSelector:
  {{ toYaml . | indent 8 }}
  {{- end }}
  {{- with .Values.affinity }}
affinity:
  {{ toYaml . | indent 8 }}
  {{- end }}
  {{- with .Values.tolerations }}
tolerations:
  {{ toYaml . | indent 8 }}
  {{- end }}
  {{ end }}
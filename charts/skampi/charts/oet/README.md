Using OET
===============

Rest client
-------------
For instructions on how to use the OET rest client, see documentation in [the SKA developer portal](https://developer.skatelescope.org/projects/observation-execution-tool/en/latest/rest_client.html).

Example allocation and configuration JSON is included with scripts. To use customised JSON either modify existing JSON
 or add a new JSON file and add path to `rest.yaml`. When referring to file, make sure to give oet-command absolute 
 path e.g. `/scripts/example_allocate.json` 

The OET rest client in SKAMPI can be accessed through Jupyter or OET SSH service.

### Jupyter
OET Jupyter can be accessed at `http://<hostname>/jupyter`. Password is oet.

Find the hostname: 
```
$ kubectl describe ingress oet-jupyter -n integration
```

In Jupyter, open a terminal by navigating to `New > Terminal`.

You can also use Jupyter to run scripts using notebooks. Example notebooks can be found in `scripts/data/notebooks`.

### SSH
Get the OET SSH service NodePort:
```
$ kubectl describe service oet-ssh-console -n integration
```

SSH to OET container:
```
$ ssh tango@localhost -p NodePort
```
PW: oet

iTango
-------------
**Note:** The SKAMPI OET iTango service is disabled by default in the values.yaml

Get the OET iTango service NodePort:
```
$ kubectl describe service oet-itango-console -n integration
```

SSH to the iTango session:
```
$ ssh tango@localhost -p NodePort
```
PW: oet

You can also start an iTango session from within the oet-ssh container:
```
$ itango3 --profile=ska
```
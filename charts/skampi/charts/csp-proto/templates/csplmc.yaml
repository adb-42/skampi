{{ if .Values.csplmc.enabled }}

---
# Device Server configurations
apiVersion: v1
kind: ConfigMap
metadata:
  name: "{{ template "csp-proto.name" . }}-configuration"
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "csp-proto.labels" . | indent 4 }}
    component: configurator
    function: deployment
    intent: enabling
    domain: self-configuration
data:
  cspconfig.json:
{{ (tpl (.Files.Glob "data/cspconfig.json").AsConfig . ) | indent 2  }}
  bootstrap.sh: |
    #/bin/sh
    json2tango -w -a -u data/cspconfig.json
    rc=$?
    if [ $rc -eq 0 ]; then
      echo "finished normally."
      exit 0
    else
      if [ $rc -eq 2 ]; then
        echo "finished with an update."
        exit 0
      else
        echo "finished with an ERROR."
        exit $rc
      fi
    fi

---
# run once Job for loading Device Server config
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ template "csp-proto.name" . }}-configurator
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "csp-proto.labels" . | indent 4 }}
    component: configurator
    function: deployment
    intent: enabling
    domain: self-configuration
spec:
  ttlSecondsAfterFinished: 100
  template:
    spec:
      initContainers:
      - name: check-databaseds-ready
        image: "{{ .Values.dsconfig.image.registry }}/{{ .Values.dsconfig.image.image }}:{{ .Values.dsconfig.image.tag }}"
        imagePullPolicy: {{ .Values.dsconfig.image.pullPolicy }}
        command:
          - /usr/local/bin/wait-for-it.sh
          - {{ if .Values.tangoDatabaseDS -}} {{ .Values.tangoDatabaseDS }}:10000 {{- else -}} databaseds-tango-base-{{ .Release.Name }}:10000 {{- end }}
          - --timeout=180
          - --strict
          - --
          - echo databaseds ready
      containers:
      - name: dsconfig
        image: "{{ .Values.dsconfig.image.registry }}/{{ .Values.dsconfig.image.image }}:{{ .Values.dsconfig.image.tag }}"
        imagePullPolicy: {{ .Values.dsconfig.image.pullPolicy }}
        command: # exit code 2 is CONFIG_APPLIED - https://github.com/MaxIV-KitsControls/lib-maxiv-dsconfig/blob/master/dsconfig/utils.py#L11 !!!! this should not be an error !!!!
          - sh
          - data/bootstrap.sh
        env:
        - name: TANGO_HOST
          value: {{ if .Values.tangoDatabaseDS -}} {{ .Values.tangoDatabaseDS }}:10000 {{- else -}} databaseds-tango-base-{{ .Release.Name }}:10000 {{- end }}
        volumeMounts:
          - name: configuration
            mountPath: data
            readOnly: true
      volumes:
        - name: configuration
          configMap:
            name: "{{ template "csp-proto.name" . }}-configuration"
      restartPolicy: Never

{{- $global := . }}
{{- range $deviceserver := .Values.deviceServers }}

---
# giving a dummy Service entry ensures that the single pod is DNS addressable
apiVersion: v1
kind: Service
metadata:
  name: {{ template "csp-proto.name" $global }}-{{ $deviceserver.name }}
  namespace: {{ $global.Release.Namespace }}
  labels:
    {{- include "csp-proto.labels" $global | indent 4 }}
    component: {{ $deviceserver.name }}
    function: {{ $deviceserver.function }}
    domain: {{ $deviceserver.domain }}
    subsystem: {{ $global.Values.subsystem }}
    intent: production
spec:
  clusterIP: None
  ports:
  - name: dummy # Actually, no port is needed.
    port: 1234
    targetPort: 1234
  selector:
    subsystem: {{ $global.Values.subsystem }}
    component: {{ $deviceserver.name }}

---
# Single Pod separate statefulset per Device Server
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ template "csp-proto.name" $global }}-{{ $deviceserver.name }}
  namespace: {{ $global.Release.Namespace }}
  labels:
    {{- include "csp-proto.labels" $global | indent 4 }}
    component: {{ $deviceserver.name }}
    function: {{ $deviceserver.function }}
    domain: {{ $deviceserver.domain }}
    subsystem: {{ $global.Values.subsystem }}
    intent: production 
spec:
  selector:
    matchLabels:
      component: {{ $deviceserver.name }}
      subsystem: {{ $global.Values.subsystem }}
  serviceName: {{ template "csp-proto.name" $global }}-{{ $deviceserver.name }}
  replicas: 1
  template:
    metadata:
      labels:
        {{- include "csp-proto.labels" $global | indent 8 }}
        component: {{ $deviceserver.name }}
        function: {{ $deviceserver.function }}
        domain: {{ $deviceserver.domain }}
        subsystem: {{ $global.Values.subsystem }}
        intent: production
    spec:
      initContainers:
      - name: check-databaseds-ready
        image: {{ $global.Values.dsconfig.image.registry }}/{{ $global.Values.dsconfig.image.image }}:{{ $global.Values.dsconfig.image.tag }}
        imagePullPolicy: {{ $global.Values.dsconfig.image.pullPolicy }}
        # 'tango_admin --check-device' will exit 0 once databaseds is alive and the dsconfig job is complete
        command:
          - retry
          - --max=10
          - --
          - tango_admin
          - --check-device
          - mid_csp/elt/master
        env:
        - name: TANGO_HOST
          value: {{ if $global.Values.tangoDatabaseDS -}} {{ $global.Values.tangoDatabaseDS }}:10000 {{- else -}} databaseds-tango-base-{{ $global.Release.Name }}:10000 {{- end }}
      containers:
      - name: deviceserver
        image: "{{ $global.Values.csplmc.image.registry }}/{{ $global.Values.csplmc.image.image }}:{{ $global.Values.csplmc.image.tag }}"
        imagePullPolicy: {{ $global.Values.csplmc.image.pullPolicy }}
        command:
          - sh
        args:
{{ toYaml $deviceserver.args | indent 10 }}
        env:
        - name: TANGO_HOST
          value: {{ if $global.Values.tangoDatabaseDS -}} {{ $global.Values.tangoDatabaseDS }}:10000 {{- else -}} databaseds-tango-base-{{ $global.Release.Name }}:10000 {{- end }}
        resources:
{{ toYaml $global.Values.resources | indent 10 }}
{{- with $global.Values.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with $global.Values.affinity }}
      affinity:
{{ toYaml . | indent 8 }}
{{- end }}
{{- with $global.Values.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
{{- end }}

{{- end }} # end of range

{{ end }}
